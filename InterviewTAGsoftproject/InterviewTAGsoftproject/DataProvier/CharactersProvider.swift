//
//  CharactersProvider.swift
//  InterviewTAGsoftproject
//
//  Created by Yana Kitsul on 3/18/20.
//  Copyright © 2020 Yana Kitsul. All rights reserved.
//

import Foundation
import UIKit

protocol ProviderResponceble {
    func handle(responce: APIResponce)
    func handle(error: String?)
}

struct CharactersProvider {
    func getNameCharacters(name: String?, page: Int = 0, delegate: ProviderResponceble) {
        let params : String = {
            var params = "?page=\(page)"
            if let name = name {
                params.append("&name=\(name)")
            }
            return params
        }()
        if let url = URL(string:"https://rickandmortyapi.com/api/character" + params) {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
       
            let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, responce, error) in
                
                if let error = error {
                    delegate.handle(error: error.localizedDescription)
                    return
                }
                
                if let data = data, let responceData = try? JSONDecoder().decode(APIResponce.self, from: data) {
                    delegate.handle(responce: responceData)
                } else {
                    delegate.handle(error: "Can not parse responce")
                }
            })
            
            task.resume()//Start loading data
        }
    }
    
}
