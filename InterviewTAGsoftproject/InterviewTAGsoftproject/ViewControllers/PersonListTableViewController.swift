//
//  PersonListTableViewController.swift
//  InterviewTAGsoftproject
//
//  Created by Yana Kitsul on 3/25/20.
//  Copyright © 2020 Yana Kitsul. All rights reserved.
//

import Foundation
import UIKit

class PersonListTableViewController: UIViewController {
    @IBOutlet weak var charactersListTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private let provider = CharactersProvider()
    
    private var dataItemsPage : Int = 0
    private var personName: String?
    private var dataItems = [Person]() {
        didSet {
            charactersListTableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        provider.getNameCharacters(name: nil, page: dataItemsPage, delegate: self)
        
        charactersListTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyTableViewCell")
        charactersListTableView.dataSource = self
        charactersListTableView.delegate = self
        
        searchBar.delegate = self
    }
    
    func reloaData() {
        dataItems = []
        dataItemsPage = 0
        provider.getNameCharacters(name: personName, page: dataItemsPage, delegate: self)
    }

    func loadNextPage() {
        dataItemsPage = dataItemsPage + 1
        provider.getNameCharacters(name: personName, page: dataItemsPage, delegate: self)
    }
}

extension PersonListTableViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataItems.count > 0 ? (dataItems.count + 1) : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyTableViewCell", for: indexPath)
        if indexPath.row < dataItems.count {
            cell.textLabel?.text = dataItems[indexPath.row].name
        } else {
            cell.textLabel?.text = "---- LOAD NEXT PAGE ----"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < dataItems.count {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let personDetailsTableViewController = storyboard.instantiateViewController(withIdentifier: "PersonDetailsTableViewController") as? PersonDetailsTableViewController {
                personDetailsTableViewController.person = dataItems[indexPath.row]
                self.show(personDetailsTableViewController, sender: nil)
            }
        } else {
            loadNextPage()
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension PersonListTableViewController: ProviderResponceble {
    func handle(responce: APIResponce) {
        DispatchQueue.main.async {
            self.dataItems = self.dataItems + responce.results
        }
    }
       
    func handle(error: String?) {
        print(error ?? "")
    }
}

extension PersonListTableViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        personName = searchBar.text
        reloaData()
        searchBar.endEditing(true)
    }
}
