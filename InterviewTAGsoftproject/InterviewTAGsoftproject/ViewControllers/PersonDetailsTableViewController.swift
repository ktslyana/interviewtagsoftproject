//
//  PersonDetailsTableViewController.swift
//  InterviewTAGsoftproject
//
//  Created by Yana Kitsul on 3/26/20.
//  Copyright © 2020 Yana Kitsul. All rights reserved.
//

import Foundation
import UIKit

class PersonDetailsTableViewController: UIViewController, UITableViewDataSource{

    @IBOutlet weak var detailTableView: UITableView!
    
    var person : Person?
    private let personDetailsValueCount = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyTableViewCell")
        detailTableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return personDetailsValueCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyTableViewCell", for: indexPath)
        switch indexPath.row {
        case 0:
            cell.textLabel?.text = "Name: \(person?.name ?? "")"
        case 1:
            cell.textLabel?.text = "Status: \(person?.status ?? "")"
        case 2:
            cell.textLabel?.text = "Species: \(person?.species ?? "")"
        case 3:
            cell.textLabel?.text = "Type: \(person?.type ?? "")"
        case 4:
            cell.textLabel?.text = "Gender: \(person?.gender ?? "")"
        default:
            break
        }
        return cell
    }
}
