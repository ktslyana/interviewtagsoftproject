//
//  APIResponce.swift
//  InterviewTAGsoftproject
//
//  Created by Yana Kitsul on 3/16/20.
//  Copyright © 2020 Yana Kitsul. All rights reserved.
//

import Foundation

struct APIResponce: Decodable {
    let info: Info
    let results: [Person]
}

struct Person: Decodable {
    let id: Int
    let name: String
    let status: String
    let species: String
    let type: String
    let gender: String
}

struct Info: Decodable {
    let count: Int
    let next: String
    let prev: String
    let pages: Int
}
